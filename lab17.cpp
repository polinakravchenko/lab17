﻿// lab17.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

using namespace std;

// C++. Демонстрація зв'язку has-a

const int MAX_STUDENTS = 5;

// Класс Faculty
class Faculty
{
private:
    string name; // назва факультету

public:

    Faculty(string _name) :name(_name)
    {
    }

    // 2.2. Конструктор без параметрів,
    // виконує виклик голового конструктора
   Faculty() : Faculty("")
    {
    }

    // 3. Методи доступу до полів класу
    string GetName() { return name; }
    void SetName (string _name)
    {
        name = _name;
    }

    // 4. Метод, який відображає інформацію про факультет
    void Print()
    {
        cout << "Name = " << name << endl;
    }
};

// Клас ComputerFaculty
class ComputerFaculty
{
    // 1. Скриті внутрішні поля
private:
    string name; // Назва
    unsigned int students;

public:
    // 2. Конструктори
    // 2.1. Конструктор з 2 параметрами - головний конструктор
    ComputerFaculty(string _name, int _students) : name(name), students(_students)
    {
    }

    // 2.2. Конструктор без параметрів,
    // виконує виклик головного конструктора
    ComputerFaculty() : ComputerFaculty("", 0) {}

    // 3. Методи доступу
    void Get(string& _name, int& _students)
    {
        _name = name;
        _students = students;
    }

    void Set(string _name, int _students)
    {
        name = _name;
        students = _students;
    }

    void Print()
    {
        cout << "Name = " << name << ", students = " << students << endl;
    }
};

class Groups
{
private:
    ComputerFaculty B[MAX_STUDENTS]; // масив об'єктів класу  - зв'язок has-a
    unsigned int courses; 
    Faculty C[MAX_STUDENTS]; 
    unsigned int groups; 

public:
    
    Groups()
    {
        courses = groups = 0;
    }

    ComputerFaculty GetFaculty (unsigned int number)
    {
     
        if (number < courses)
            return B[number];
        else
        {
            cout << "Error. Incorrect course number." << endl;
            return ComputerFaculty("", 0);
        }
    }

    void AddCourse(string name, unsigned int students)
    {

        if ((groups + courses) < MAX_STUDENTS)
        {
            courses++; 
            B[courses - 1].Set(name, students); 
            cout << "A new course is added!" << endl;
        }
        else
        {
            cout << "Cannot add a new course. Sorry" << endl;
            return;
        }
    }

    void DelCourse(unsigned int number)
    {
        if (number < courses )
        {
        
            for (int i = number; i < courses - 1; i++)
                B[i] = B[i + 1];
            courses--;
        }
    }


    Faculty GetGroup(unsigned int number)
    {
        if (number < groups)
            return C[number];
        else
        {
            cout << "Error. Incorrect naming of a group." << endl;
            return Faculty("");
        }
    }

    void AddGroup(string name)
    {
   
        if ((groups + courses) < MAX_STUDENTS)
        {
            groups++;
            C[groups - 1].SetName(name); 
            cout << "A new group is added!" << endl;
        }
        else
        {
            cout << "Cannot add a new group. Sorry" << endl;
            return;
        }
    }


    void DelGroup(unsigned int number)
    {
        if (number < groups)
        {
         
            for (int i = number; i < groups - 1; i++)
                C[i] = C[i + 1];
           groups--;
        }
    }

    void Print()
    {
        cout << "Info about faculty:" << endl;
        cout << "Numbering = " << courses << endl;
        if (courses > 0)
        {
            cout << "Info about courses:" << endl;
            for (int i = 0; i < courses; i++)
            {
                B[i].Print();
            }
        }
        cout << "Groups = " << groups << endl;
        if (groups > 0)
        {
            cout << "Info about groups:" << endl;
            for (int i = 0; i < groups; i++)
            {
                C[i].Print();
            }
        }
    }
};

void main()
{
    Groups bs;


    bs.AddCourse("First", 105);
    bs.AddCourse("Second", 205);
    bs.AddGroup("Computer Engineering");
    bs.AddGroup("Computer Engineering");


    bs.Print();
}
